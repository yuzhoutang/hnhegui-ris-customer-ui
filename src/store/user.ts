import { defineStore } from 'pinia'

interface useStore {
  role: 'manager' | 'assistant'
}

export const useUserStore = defineStore('userIndex', {
  state: (): useStore => {
    return {
      role: 'manager',
    }
  },
})
