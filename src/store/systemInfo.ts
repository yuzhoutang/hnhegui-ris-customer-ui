import { defineStore } from 'pinia'
import Taro from '@tarojs/taro'
import { getMenuButtonBounding } from '@/platforms/system'

export const useSystemInfoStore = defineStore('systemInfoIndex', {
  state: () => {
    return {
      systemInfo: {} as any,
    }
  },

  getters: {},

  actions: {
    async initSystemInfo() {
      // 获取胶囊的高度
      const menuButton = Taro.getMenuButtonBoundingClientRect()

      const { statusBarHeight, platform, ...other } = await Taro.getSystemInfo()
      const { top, height } = (await getMenuButtonBounding()) || {}
      const navigationBarHeight =
        top !== 0 && height !== 0
          ? (top! - statusBarHeight!) * 2 + height!
          : platform === 'android'
          ? 48
          : 40
      let otherProp = {}
      if (Taro.canIUse('getAccountInfoSync')) {
        const info = await Taro.getAccountInfoSync()
        // other.mpVersion = info?.miniProgram?.version
        otherProp = {
          ...other,
          mpVersion: info?.miniProgram?.version,
          menuButton,
        }
      }
      this.systemInfo = {
        statusBarHeight,
        navigationBarHeight,
        ...otherProp,
      }
      console.log('系统参数', this.systemInfo)
    },
  },
})
