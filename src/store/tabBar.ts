// import Taro from '@tarojs/taro'
import { defineStore } from 'pinia'
// import { useUserStore } from '@/store/user'

export const useTabBarStore = defineStore('tabBarIndex', {
  state: () => {
    return {
      selected: 0,
      // 店长tabBar
      shopManagerList: [
        {
          pagePath: '/pages/news/index',
          text: '资讯',
          iconPath: '../assets/icon/news.png',
          selectedIconPath: '../assets/icon/news-active.png',
        },
        {
          pagePath: '/pages/purchaseAndStocking/index',
          text: '采购铺货',
          iconPath: '../assets/icon/purchase.png',
          selectedIconPath: '../assets/icon/purchase-active.png',
        },
        {
          pagePath: '/pages/scanAndVerify/index',
          text: '',
          bulge: true,
          iconPath: '../assets/icon/scan.png',
          selectedIconPath: '../assets/icon/scan.png',
        },
        {
          pagePath: '/pages/pointsShop/index',
          text: '积分商城',
          iconPath: '../assets/icon/points-shop.png',
          selectedIconPath: '../assets/icon/points-shop-active.png',
        },
        {
          pagePath: '/pages/userCenter/index',
          text: '个人中心',
          iconPath: '../assets/icon/user-center.png',
          selectedIconPath: '../assets/icon/user-center-active.png',
        },
      ],
      // 店员tabBar
      shopAssistantList: [
        {
          pagePath: '/pages/purchaseAndStocking/index',
          text: '采购铺货',
          iconPath: '../assets/icon/purchase.png',
          selectedIconPath: '../assets/icon/purchase-active.png',
        },
        {
          pagePath: '/pages/scanAndVerify/index',
          text: '',
          bulge: true,
          iconPath: '../assets/icon/scan.png',
          selectedIconPath: '../assets/icon/scan.png',
        },
        {
          pagePath: '/pages/userCenter/index',
          text: '个人中心',
          iconPath: '../assets/icon/user-center.png',
          selectedIconPath: '../assets/icon/user-center-active.png',
        },
      ],
    }
  },
  actions: {},
})
