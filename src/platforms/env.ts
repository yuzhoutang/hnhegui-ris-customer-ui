const isWeApp = process.env.TARO_ENV === 'weapp'

const isH5 = process.env.TARO_ENV === 'h5'

export { isWeApp, isH5 }
