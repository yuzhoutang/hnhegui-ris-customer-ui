import Taro from '@tarojs/taro'
import { isH5 } from './env'
// import api from '../services/api'
import isAfterTheVersion from '../utils/diff'

// export async function login(payload = {}) {
//   if (isH5) {
//   } else {
//     const entryMethod = Taro.getStorageSync('userFromTypeCode')
//     const config = await Taro.login()
//     const result = await api.postCustomerLoginWxLogin({
//       code: config.code,
//       entryMethod,
//       ...payload,
//     })
//     Taro.setStorageSync('login_result', result.data)
//     return result
//   }
// }

export function getMenuButtonBounding() {
  if (isH5) {
    return {
      top: 0,
      height: 60,
    }
  } else {
    return isAfterTheVersion('2.1.0')
      ? Taro.getMenuButtonBoundingClientRect()
      : null
  }
}
