import { createApp } from 'vue'
import { createPinia } from 'pinia'
import '@/services/index_backup'
import initSystemInfo from './utils/initSystemInfo'
import { useLoginJobScheduler } from '@/services/schedule'
import { getToken, login } from '@/api/customer'

import './app.scss'
import 'uno.css'

const App = createApp({
  componentDidShow() {
    // 设置前置job List
    useLoginJobScheduler().setPreJob(
      [
        // 刷新token
        async () => {
          await getToken()
        },
        async () => {
          await login({})
        },
      ],
      false
    )
    useLoginJobScheduler().setPreJobRefresher(() => {
      return false // result?.code === 403
    })
  },
  onLaunch() {
    // 初始化系统参数
    initSystemInfo()
  },
})

App.use(createPinia())

export default App
