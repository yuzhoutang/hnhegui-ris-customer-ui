export default {
  pages: [
    'pages/news/index',
    'pages/purchaseAndStocking/index',
    'pages/scanAndVerify/index',
    'pages/pointsShop/index',
    'pages/userCenter/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black',
    navigationStyle: 'custom',
  },
  tabBar: {
    color: '#999',
    custom: true,
    selectedColor: '#E74E16',
    backgroundColor: '#fff',
    borderStyle: 'black',
    list: [
      {
        pagePath: 'pages/news/index',
        text: '资讯',
        iconPath: './assets/icon/news.png',
        selectedIconPath: './assets/icon/news-active.png',
      },
      {
        pagePath: 'pages/purchaseAndStocking/index',
        text: '采购铺货',
        iconPath: './assets/icon/purchase.png',
        selectedIconPath: './assets/icon/purchase-active.png',
      },
      {
        pagePath: 'pages/scanAndVerify/index',
        text: '',
        bulge: true,
        iconPath: './assets/icon/scan.png',
        selectedIconPath: './assets/icon/scan.png',
      },
      {
        pagePath: 'pages/pointsShop/index',
        text: '积分商城',
        iconPath: './assets/icon/points-shop.png',
        selectedIconPath: './assets/icon/points-shop-active.png',
      },
      {
        pagePath: 'pages/userCenter/index',
        text: '个人中心',
        iconPath: './assets/icon/user-center.png',
        selectedIconPath: './assets/icon/user-center-active.png',
      },
    ],
  },
  subPackages: [
    {
      root: 'packages/login',
      name: 'login',
      pages: ['loginPage'],
    },
    {
      root: 'packages/userCenter',
      name: 'userCenter',
      pages: ['accumulatedIncome', 'personnelManagement', 'userProfile'],
    },
    {
      root: 'packages/shop',
      name: 'shop',
      pages: ['bindShop', 'shopList'],
    },
    {
      root: 'packages/order',
      name: 'order',
      pages: [
        'purchaseOrder',
        'claimPrizeReturnGoods',
        'claimPrizeReturnDetail',
        'claimPrizeReturnInfo',
      ],
    },
  ],
  preloadRule: {},
}
