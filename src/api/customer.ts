import runHttpsJob, { Https } from '@/services/index'

// 用户登录 无需先调用前置任务
export function login(data) {
  return Https({
    url: '/login/login1',
    method: 'POST',
    data,
  })
}

export function getToken() {
  return runHttpsJob({
    url: '/login/token123',
    method: 'GET',
  })
}
