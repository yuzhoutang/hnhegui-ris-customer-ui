import runHttpsJob from '@/services/index'

export function getWxMaOrdercallFlagorder(data) {
  return runHttpsJob({
    url: '/wx/ma/ordercall/flagorder',
    method: 'GET',
    data,
  })
}

export function getWxMaGoodsList(data) {
  return runHttpsJob({
    url: '/wx/ma/goods/list',
    method: 'GET',
    data,
  })
}

export function postWxMaOrdercallCall(data) {
  return runHttpsJob({
    url: '/wx/ma/ordercall/call',
    method: 'POST',
    data,
  })
}

export function getWxMaCustomerBind_list() {
  return runHttpsJob({
    url: '/wx/ma/customer/bind_list',
    method: 'GET',
  })
}
