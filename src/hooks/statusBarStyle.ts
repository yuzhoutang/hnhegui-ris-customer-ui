import { computed } from 'vue'
import { useSystemInfoStore } from '@/store/systemInfo'

const systemInfoStore = useSystemInfoStore()

export function usePaddingTopStyle() {
  return computed(() => {
    return {
      paddingTop: systemInfoStore.systemInfo.statusBarHeight + 'px',
    }
  })
}
