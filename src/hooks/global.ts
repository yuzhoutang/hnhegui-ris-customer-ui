import { reactive, ref } from 'vue'
import { getWxMaCustomerBind_list } from '@/api/purchaseAndStocking'

export const userInfo = reactive({})

export const shops = ref([])

export const applyList = reactive([])

export const currentApply = reactive({})

export const setUserInfo = userData => {
  Object.assign(userInfo, userData)
}

/**
 * 查询店铺信息
 */
export const queryShop = async () => {
  const result = await getWxMaCustomerBind_list()
  if (result.code === 200) {
    shops.value = result.data.customerList
  }
}
