import { useBridge } from 'ts-autoapi'
import Taro from '@tarojs/taro'
import { errorInterceptor, encodeInterceptor } from './interceptors'
import { useLoginJobScheduler } from './schedule'
import { NETWORK_ERROR } from '@/utils/logicString'

/**
 * 注入请求拦截器
 */
Taro.addInterceptor(encodeInterceptor)
Taro.addInterceptor(errorInterceptor)

/**
 * 网络出现波动给用户友好提示
 */
Taro.onNetworkStatusChange(async res => {
  if (res.isConnected) {
  } else {
    await Taro.showToast({
      title: NETWORK_ERROR,
      icon: 'none',
      duration: 5000,
    })
  }
})

/**
 * 注入 ts-autoapi 请求桥接器，把 api 调用的方法全部代理到微信小程序请求库上
 */
useBridge().setRequestHandler(config => {
  return useLoginJobScheduler().runJob(async () => {
    try {
      const result = await Taro.request({
        data: config.body || config.query,
        url: process.env.TARO_APP_REQUEST_URL + config.url,
        header: config.headers || {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        method: config.method as unknown as keyof Taro.request.Method,
      })
      // 在真机上显示请求参数等信息，便于调试
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      if (Taro.getAppBaseInfo?.().host?.env !== 'devtools') {
        console.warn(config.url, config.body || config.query, result)
      }
      return result.data
    } catch (e: any) {
      if (e.code == null && e.errMsg != null) {
        await Taro.showToast({
          title: NETWORK_ERROR,
          icon: 'none',
          duration: 5000,
        })
      }
      throw e
    }
  })
})
