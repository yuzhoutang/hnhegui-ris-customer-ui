import { test, expect, vi } from 'vitest'
import { JobScheduleImpl, RetryExceededError } from '../schedule'

test('普通任务需要在前置任务完成后再执行', async () => {
  const job = new JobScheduleImpl()
  const fn = vi.fn()

  job.setPreJob(async () => {
    await Promise.resolve()
    fn(1)
  })

  expect(fn).toHaveBeenCalledTimes(0)

  const tasks = [job.runJob(() => fn(2)), job.runJob(() => fn(3))]

  await Promise.all(tasks)

  expect(fn).toHaveBeenCalledTimes(3)
  expect(fn).toHaveBeenNthCalledWith(1, 1)
  expect(fn).toHaveBeenNthCalledWith(2, 2)
  expect(fn).toHaveBeenNthCalledWith(3, 3)

  job.runJob(() => fn(4))
  expect(fn).toHaveBeenNthCalledWith(4, 4)
})

test('错误自动重试和取消机制测试', async () => {
  const job = new JobScheduleImpl()
  const fn = vi.fn()

  job.setPreJob(async () => {
    await Promise.resolve()
    return Promise.reject()
  })

  expect(fn).toHaveBeenCalledTimes(0)
  expect(job.runJob(() => fn(1))).rejects.toBeInstanceOf(RetryExceededError)
  expect(job.runJob(() => fn(2))).rejects.toBeInstanceOf(RetryExceededError)
})

test('如果token过期，需要能自动重新登录', async () => {
  const job = new JobScheduleImpl()
  const fn = vi.fn()

  job.setPreJob(async () => {
    fn('login')
    await Promise.resolve()
  })

  let runTiming = 0
  job.setPreJobRefresher(() => {
    runTiming++
    // 执行三次，需要重新登录
    return runTiming === 3
  })

  await Promise.all([job.runJob(() => {}), job.runJob(() => {})])

  expect(fn).toHaveBeenCalledTimes(1)
  expect(fn).toHaveBeenLastCalledWith('login')

  await job.runJob(() => fn('call'))

  // 调用这一次登录后，发现已经过期了
  expect(fn).toHaveBeenNthCalledWith(2, 'call')
  // 触发重新登录
  expect(fn).toHaveBeenNthCalledWith(3, 'login')
  // 登录前未完成的任务将继续执行
  expect(fn).toHaveBeenNthCalledWith(4, 'call')
})
