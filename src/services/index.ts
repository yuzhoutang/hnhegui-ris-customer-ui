import Taro from '@tarojs/taro'
import { errorInterceptor, encodeInterceptor } from './interceptors'
import { useLoginJobScheduler } from './schedule'
import { type HttpsConfig } from './types'
import { NETWORK_ERROR } from '@/utils/logicString'

/**
 * 注入请求拦截器
 */
Taro.addInterceptor(encodeInterceptor)
Taro.addInterceptor(errorInterceptor)

/**
 * 网络出现波动给用户友好提示
 */
Taro.onNetworkStatusChange(async res => {
  if (res.isConnected) {
  } else {
    await Taro.showToast({
      title: NETWORK_ERROR,
      icon: 'none',
      duration: 5000,
    })
  }
})

async function httpsJob(config: HttpsConfig) {
  try {
    const result = await Taro.request({
      data: config.data,
      url: process.env.TARO_APP_REQUEST_URL + config.url,
      header: config.headers || {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: config.method as unknown as keyof Taro.request.Method,
    })
    // 在真机上显示请求参数等信息，便于调试
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (Taro.getAppBaseInfo?.().host?.env !== 'devtools') {
      console.warn(config.url, config.data, result)
    }
    return result.data
  } catch (e: any) {
    if (e.code == null && e.errMsg != null) {
      await Taro.showToast({
        title: NETWORK_ERROR,
        icon: 'none',
        duration: 5000,
      })
    }
    throw e
  }
}

/**
 * 接口请求，会将此次请求加入调度器调入
 * @param config
 * @returns
 */
export default function runHttpsJob(config: HttpsConfig) {
  return useLoginJobScheduler().runJob(() => {
    return httpsJob(config)
  })
}

export function Https(config: HttpsConfig) {
  return httpsJob(config)
}
