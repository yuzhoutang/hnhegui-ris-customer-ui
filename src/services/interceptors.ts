// loading 拦截器，对当前请求的path进行监听，
import Taro from '@tarojs/taro'
import { keys } from 'lodash-es'
import md5 from 'md5'
import { isObject } from '@/utils/typeIs'
import { SERVICE_ERROR } from '@/utils/logicString'

export function getMd5Header(header, data) {
  // 加入毫秒时间戳
  const params = Object.assign({ timestamp: Date.now() }, data)

  // 填充码
  const fillCode =
    header?.Authorization != null ? header.Authorization.slice(-6) : 'cchs'

  // 排除为空的参数，把对象和数组stringify
  const values = keys(params)
    .sort()
    .map(i => {
      if (params[i] != null) {
        if (isObject(params[i]) || Array.isArray(params[i])) {
          return JSON.stringify(params[i])
        }
        return params[i]
      }
    })
    .filter(i => i != null)
    .join('')
  // 用来生成md5的基础字符串
  const md5base = [values, fillCode].filter(i => i != null).join('')

  return {
    sign: md5(md5base),
    timestamp: params.timestamp,
  }
}

export const tokenStore: any = {}

/**
 * 加入鉴权凭证
 */
export function getTokenHeader() {
  if (tokenStore.access_token != null) {
    return { Authorization: `CusBearer ${tokenStore.access_token}` }
  } else {
    return {}
  }
}

/**
 * 加密拦截器，对请求参数进行加密
 * @see https://hegui.coding.net/p/cchs/requirements/issues/4367/detail
 */
export async function encodeInterceptor(chain: Taro.Chain) {
  Object.assign(
    chain.requestParams.header,
    getMd5Header(chain.requestParams.header, chain.requestParams.data)
  )

  return chain.proceed(chain.requestParams)
}

// 请求错误拦截器
export async function errorInterceptor(chain: Taro.Chain) {
  const result = await chain.proceed(chain.requestParams)

  // http 状态码
  switch (result.data.code) {
    // 后端异常，进行兜底。展示友好错误文案
    case 500:
      Taro.showToast({
        title: SERVICE_ERROR,
        icon: 'none',
      })
      break
    case 501:
      Taro.showToast({
        title: result.data.msg,
        icon: 'none',
      })
      break
  }
  return result
}
