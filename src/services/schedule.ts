type Job = () => any

enum TaskState {
  initial,
  running,
  done,
}

/**
 * 发布订阅模型
 */
export class EventCenter {
  handlers = new Map<string | symbol, ((...args: any[]) => void)[]>()

  on(event: string | symbol, callback: (...args: any[]) => void) {
    const handlers = this.handlers.get(event)
    if (!handlers) {
      this.handlers.set(event, [callback])
    }
  }

  once(event: string | symbol, callback: (...args: any[]) => void) {
    const handlers = this.handlers.get(event)
    if (!handlers) {
      this.handlers.set(event, [callback])
    } else {
      handlers.push(callback)
    }
  }

  off(event: string | symbol, callback?: (...args: any[]) => void) {
    if (!callback) {
      this.handlers.delete(event)
      return
    }
    const handlers = this.handlers.get(event)
    if (!handlers) {
      return
    }
    const index = handlers.indexOf(callback)
    if (index >= 0) {
      handlers.splice(index, 1)
    }
  }

  trigger(event: string | symbol, ...args: any[]) {
    const handlers = this.handlers.get(event)
    if (handlers) {
      handlers.forEach(handler => handler(...args))
    }
  }
}

/**
 * 错误重试次数超出了
 */
export class RetryExceededError extends Error {}

/**
 * 任务调度器，主要用来处理自动登录, 基于发布订阅模式并对外隐藏了发布订阅的特性
 *
 * 功能：在多入口小程序，通常需要多次处理登录逻辑，可以通过此调度器来进行无感登录和自动刷新token
 *
 * @example
 *
 * ```jsx
 * // 设置前置任务，必须要先获取token之后的接口才能调通
 * useLoginJobScheduler().setPreJob(() => {
 *   const result = await Taro.login()
 *   const { token } = await postLogin({ code: result.code })
 *   globalState.token = token
 * })
 *
 * // 设置前置任务刷新机制，当接口返回的code为 403 时，将会再次触发 preJob 运行
 * useLoginJobScheduler().setPreJobRefresher((result) => {
 *   return result.code === 403
 * })
 *
 * // 会保证在运行完 preJob 后运行完
 * const result = useLoginJobScheduler().runJob(() => {
 *   return getUserInfo({
 *     headers: { token: globalState.token }
 *   })
 * })
 *
 * // 即使同步运行多个普通任务依然会等待 preJob
 * const result = useLoginJobScheduler().runJob(() => {
 *   return getGoodsList({
 *     headers: { token: globalState.token }
 *   })
 * })
 *  *
 * ```
 */
export class JobScheduleImpl {
  constructor(
    /**
     * 前置任务重试次数
     */
    private retry = 3,
    private doneMessage = Symbol(),
    private eventCenter = new EventCenter()
  ) {}

  /**
   * 前置任务是否完成
   */
  private preJobState = TaskState.initial

  /**
   * 是否需要刷新
   */
  private needRefresh?: (result: any) => boolean

  /**
   * 前置任务
   */
  // private preJob?: Job
  private preJobList: Job[] = []

  /**
   * 完成一个任务
   */
  private async flushJob(job: Job) {
    if (!this.preJobList.length) {
      throw new Error('请先设置前置任务')
    }
    // 如果任务进行中，等待 eventbus 通知后再运行
    if (this.preJobState === TaskState.running) {
      return new Promise((resolve, reject) => {
        this.eventCenter.once(this.doneMessage, (errored?: boolean) => {
          if (errored) {
            reject(new RetryExceededError())
          } else {
            resolve(job())
          }
        })
      })
    }

    // this.preJobState = TaskState.running

    const retryLimit = this.retry
    // 执行前置任务，并可重试指定次数
    while (this.retry > 0) {
      try {
        await this.flushPreJobs()
        this.preJobState = TaskState.done
        break
      } catch {
        // 发生错误重置状态
        this.preJobState = TaskState.initial
        this.retry--
      }
    }
    // 触发完成事件
    this.eventCenter.trigger(this.doneMessage, this.retry === 0)
    this.retry = retryLimit
    return job()
  }

  /**
   * 设置前置任务
   * @param job 前置任务
   * @param immediate 是否立即执行前置任务，如果为否，将会在新的任务执行前才执行
   * TODO：多前置任务支持
   */
  // setPreJob_backup(job: Job, immediate = true): Promise<any> | void {
  //   this.preJob = job
  //   if (immediate) {
  //     this.flushJob(() => {})
  //   }
  // }

  /**
   * 设置多前置任务
   * @param job前置任务或前置任务List
   * @param immediate 是否立即执行前置任务，如果为否，将会在新的任务执行前才执行
   */
  setPreJob(job: Job[] | Job, immediate = true): Promise<any> | void {
    if (Array.isArray(job)) {
      this.preJobList.push(...job)
    } else {
      this.preJobList.push(job)
    }
    if (immediate) {
      this.flushJob(() => {})
    }
  }

  /**
   * 刷新多前置任务
   */
  async flushPreJobs() {
    const promiseList: Promise<any>[] = []
    // 前置任务可能也会执行runJob，在runJob中直接执行该前置任务
    this.preJobState = TaskState.done
    this.preJobList.forEach(preJob => {
      promiseList.push(preJob())
    })
    this.preJobState = TaskState.running
    return Promise.all(promiseList)
  }

  /**
   * 设置前置任务刷新器
   */
  setPreJobRefresher(needRefresh: (result: any) => boolean) {
    this.needRefresh = needRefresh
  }

  /**
   * 运行普通任务
   */
  async runJob(job: Job): Promise<any> {
    // 如果当前前置任务没有执行完，加入第二队列，如果前置任务执行完了就直接执行
    try {
      const result =
        this.preJobState !== TaskState.done
          ? await this.flushJob(job)
          : await job()
      // 如果需要刷新任务，修改状态后，把当前任务再次加入队列
      if (this.needRefresh?.(result)) {
        this.preJobState = TaskState.initial
        return this.runJob(job)
      }
      return result
    } catch (e) {
      throw e
    }
  }
}

/**
 * 调度器唯一实例
 */
let scheduler: JobScheduleImpl

/**
 * 登录调度器，用于自动登录
 */
export function useLoginJobScheduler() {
  return (scheduler ??= new JobScheduleImpl())
}
