// 定义请求方法枚举
export type HttpsConfig = {
  data?: object
  method: keyof Taro.request.Method
  url: string
  headers?: object
}
