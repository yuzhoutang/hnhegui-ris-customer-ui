/* eslint-disable */
// @ts-nocheck
/**
 * 客户基本信息 的请求对像
 */
export interface CustomerLoginRequest {
  /**
   * appId
   */
  appId?: string

  /**
   * 授权渠道(1微信2app)
   */
  authorizeChannel?: number

  /**
   * 渠道hash
   */
  channelHash?: string

  /**
   * 渠道openid
   */
  channelOpenId?: string

  /**
   * 渠道time
   */
  channelTime?: string

  /**
   * 渠道用户id
   */
  channelUserId?: string

  code?: string

  /**
   * 客户Id
   */
  customerId?: number

  entryMethod?: string

  /**
   * openid
   */
  openid?: string

  /**
   * unionid
   */
  unionid?: string

  /**
   * 用户渠道(kww_vip-口味王)
   */
  userChannel?: string

}


