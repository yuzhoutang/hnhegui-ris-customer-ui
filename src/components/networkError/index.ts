import type { AppContext, ComponentPublicInstance, VNode } from 'vue'
import { createVNode, render } from 'vue'

import NetworkErrConstructor from './index.vue'

const getAppendToElement = (props: any): HTMLElement => {
  const appendTo: HTMLElement | null = document.body
  return appendTo
}

const initInstance = (
  props: any,
  container: HTMLElement,
  appContext: AppContext | null = null
) => {
  const vnode = createVNode(NetworkErrConstructor, props, null)

  console.log('vnode', vnode)
  console.log('container', container)

  vnode.appContext = appContext
  render(vnode, container)
  getAppendToElement(props).appendChild(container)

  console.log('-----添加了容器到body之后----', document.body)

  return vnode.component
}

const genContainer = () => {
  return document.createElement('view')
}

const showDialog = (appContext?: AppContext | null) => {
  const container = genContainer()
  const instance = initInstance(null, container, appContext)
  return instance
}

export default showDialog
