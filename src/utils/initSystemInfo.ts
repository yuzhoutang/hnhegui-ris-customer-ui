import { useSystemInfoStore } from '@/store/systemInfo'
// import { useTabBarStore } from '@/store/tabBar'

export default () => {
  const systemInfoStore = useSystemInfoStore()
  // const tabBarStore = useTabBarStore()
  systemInfoStore.initSystemInfo()
  // tabBarStore.initPage()
}
