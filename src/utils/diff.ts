import Taro from '@tarojs/taro'

export default function isAfterTheVersion(v) {
  const current: any = Taro.getSystemInfoSync()?.SDKVersion?.split('.')
  const last = v.split('.')
  const len = Math.max(v.length, current.length)

  while (last.length < len) {
    last.push('0')
  }
  while (current.length < len) {
    current.push('0')
  }

  for (let i = 0; i < len; i++) {
    const num1 = parseInt(current[i])
    const num2 = parseInt(last[i])

    if (num1 > num2) {
      return true
    } else if (num1 < num2) {
      return false
    }
  }

  // 如果两个版本完全相同
  return true
}
