// 是否为对象格式
export function isObject(arg: any) {
  return Object.prototype.toString.call(arg) === '[object Object]'
}
