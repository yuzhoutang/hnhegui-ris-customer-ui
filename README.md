# hnhegui-mp-tpl

## 项目描述

Taro 小程序模板，基于 taro + webpack5 + vue3 + ts + unocss + vitest 模板项目

## 开发规范

需遵询项目所配置 eslint、prettier 代码风格，变量名有意义、无拼写错误。文件命名需使用驼峰（部分路由名限制除外）。

## 运行环境

node16 + pnpm

## 发布流程及 CI/CD 说明

TODO

## 接口调用函数及类型自动生成

在具有 openapi json 文档的情况下，可以一键生成接口请求函数，和接口地址。但是需要注意处理代码冲突。

```bash
# 完整命令，已添加到package.json scripts中，可通过运行 `pnpm gen:api` 命令来生成接口请求函数及类型申明
ts-autoapi gen --doc <openapi doc地址> --output <输出接口请求文件地址> --model <输出实体类型文件路径>

# 示例
ts-autoapi gen --doc http://192.168.12.216:8080/capi/v2/api-docs --output src/services/apis --model src/services/apis/models
```

生成函数采用模板字符串形式函数调用，比普通函数箭头函数代码体积会更轻一些。

```js
// 生成示例
const getUser = api`GET /user/{userId}` // param in path 参数会自动处理

function getUser(userId, config) {
  return request.get('/user/' + userId, config)
}

getUser(1, {
  /* request options */
}) // 两者调用方式是一致的, 但模板语法编译后可节省一半代码体积
```

ts-autoapi 为保证通用性，没有封装任何请求库, 所以需要注册请求桥接器，这部分已在 `services/index.ts` 中实现

## 开发环境构建加速及清理缓存

为加速开发环境的调试体验，小程序采用 webpack5 构建并且开启了缓存。在较极端情况下如遇到生成内容和代码不符合，可考虑删除`node_modules/.cache` 文件夹，和项目根目录下 `.swc` 文件夹，再重新构建。

## unocss 用法

小程序 CSS 语法为一个浓缩集合，并不能支持浏览器语法。所以 unocss 也有一些不能使用的地方。但基本需求开发是没有问题的。

单位采用 rpx, 如。

```html
<view class="w750rpx"></view>
<!-- 全屏幕宽 -->
```
