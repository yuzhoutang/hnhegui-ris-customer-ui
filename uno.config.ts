import {
  defineConfig,
  transformerDirectives,
  transformerVariantGroup,
  transformerCompileClass,
  type PresetOrFactory,
  type SourceCodeTransformer,
} from 'unocss'
import {
  presetApplet,
  transformerApplet,
  transformerAttributify,
  // 因为默认单位为rpx, 如果需要打包H5，需要配置 rpx to rem 转换规则
  // presetRemRpx
} from 'unocss-applet'
import presetWeapp from 'unocss-preset-weapp'
import {
  extractorAttributify,
  transformerClass,
} from 'unocss-preset-weapp/transformer'
const { presetWeappAttributify } = extractorAttributify()

export default defineConfig({
  content: {
    pipeline: {
      include: [/\.(vue|[jt]sx)($|\?)$/],
    },
  },
  presets: [
    presetApplet(),
    // px 转 rpx
    presetWeapp({
      isH5: process.env.TARO_ENV === 'h5',
      platform: 'taro',
      taroWebpack: 'webpack5',
      designWidth: 750,
      deviceRatio: {
        640: 2.34 / 2,
        750: 1,
        828: 1.81 / 2,
        375: 2 / 1,
      },
    }) as PresetOrFactory<object>,
    // presetWeappAttributify(),
    // presetIcons(),
  ],
  transformers: [
    transformerApplet(),
    transformerAttributify(),
    transformerCompileClass(),
    transformerDirectives(),
    transformerVariantGroup(),
    transformerClass() as unknown as SourceCodeTransformer,
  ],
  shortcuts: {
    // 配置快捷样式指令
    'base-text': 'text-base text-gray-500',
    'base-text-xl': 'text-xl text-gray-500',
    'text-2line': 'line-clamp-2 overflow-hidden text-ellipsis',
    'flex-row-center': 'flex flex-center',
    'flex-column-center': 'flex flex-col flex-center',
    'main-page': 'w-100vw h-100vh o-hidden b-box f-family',
  },
  rules: [
    // ** 尺寸相关 **
    [/^w-(\d+)$/, ([_, num]) => ({ width: `${num}px` })],
    // 高度
    [/^h-(\d+)$/, ([_, num]) => ({ height: `${num}px` })],
    // 宽度100%
    [/^w-100p$/, () => ({ width: '100%' })],
    // 高度100%
    [/^h-100p$/, () => ({ height: '100%' })],
    // 宽度100vw
    [/^w-100vw$/, () => ({ width: '100vw' })],
    // 高度100vh
    [/^h-100vh$/, () => ({ height: '100vh' })],

    // ** 字体相关 **
    [/^f-s-(\d+)$/, ([, size]) => ({ 'font-size': `${size}px` })],
    [/^f-w-(\d+)$/, ([, weight]) => ({ 'font-weight': weight })],
    [/^l-h-(\d+)$/, ([, height]) => ({ 'line-height': `${height}px` })],
    [/^l-s-(\d+)$/, ([, spacing]) => ({ 'letter-spacing': `${spacing}px` })],
    // 字体颜色
    [/^color-(\w+)$/, ([, color]) => ({ color: `#${color}` })],
    [/^m-([\.\d]+)$/, ([_, num]) => ({ margin: `${num}px` })],
    [/^mt-([\.\d]+)$/, ([_, num]) => ({ 'margin-top': `${num}px` })],
    [/^mb-([\.\d]+)$/, ([_, num]) => ({ 'margin-bottom': `${num}px` })],
    [/^ml-([\.\d]+)$/, ([_, num]) => ({ 'margin-left': `${num}px` })],
    [/^mr-([\.\d]+)$/, ([_, num]) => ({ 'margin-right': `${num}px` })],
    // font-family: PingFang SC, PingFang SC;
    ['f-family', { 'font-family': 'PingFang SC, PingFang SC' }],

    // ** flex/位置 相关 **
    ['b-box', { 'box-sizing': 'border-box' }],
    ['flex', { display: 'flex' }],
    ['flex-1', { flex: '1' }],
    ['j-c-start', { 'justify-content': 'flex-start' }],
    ['j-c-end', { 'justify-content': 'flex-end' }],
    ['j-c-between', { 'justify-content': 'space-between' }],
    ['j-c-around', { 'justify-content': 'space-around' }],
    ['j-c-center', { 'justify-content': 'center' }],
    ['a-i-start', { 'align-items': 'flex-start' }],
    ['a-i-end', { 'align-items': 'flex-end' }],
    ['a-i-center', { 'align-items': 'center' }],
    ['a-i-stretch', { 'align-items': 'stretch' }],
    ['flex-row', { display: 'flex', 'flex-direction': 'row' }],
    ['flex-col', { display: 'flex', 'flex-direction': 'column' }],
    ['flex-wrap', { 'flex-wrap:': 'wrap' }],
    ['flex-nowrap', { 'flex-wrap': 'nowrap' }],
    ['flex-center', { 'justify-content': 'center', 'align-items': 'center' }],
    ['flex-between', { 'justify-content': 'space-between' }],
    // 绝对定位
    ['absolute', { position: 'absolute' }],
    // 相对定位
    ['relative', { position: 'relative' }],
    // 绝对定位
    ['fixed', { position: 'fixed' }],
    // 位置 top
    [/^top-(\d+)$/, ([_, num]) => ({ top: `${num}px` })],
    // 位置 bottom
    [/^bottom-(\d+)$/, ([_, num]) => ({ bottom: `${num}px` })],
    // 位置 left
    [/^left-(\d+)$/, ([_, num]) => ({ left: `${num}px` })],
    // 位置 right
    [/^right-(\d+)$/, ([_, num]) => ({ right: `${num}px` })],
    // padding
    [/^p-(\d+)$/, ([_, num]) => ({ padding: `${num}px` })],
    // padding-top
    [/^pt-(\d+)$/, ([_, num]) => ({ 'padding-top': `${num}px` })],
    // padding-bottom
    [/^pb-(\d+)$/, ([_, num]) => ({ 'padding-bottom': `${num}px` })],
    // padding-left
    [/^pl-(\d+)$/, ([_, num]) => ({ 'padding-left': `${num}px` })],
    // padding-right
    [/^pr-(\d+)$/, ([_, num]) => ({ 'padding-right': `${num}px` })],
    [/^text-center$/, () => ({ 'text-align': 'center' })],

    // ** 边框相关 **
    // 圆角
    [/^b-r-(\d+)$/, ([_, num]) => ({ 'border-radius': `${num}px` })],
    // 背景颜色
    [/^bg-color-(\w+)$/, ([_, color]) => ({ 'background-color': `#${color}` })],
    // 透明度
    [/^bg-opacity-(\d+)$/, ([_, num]) => ({ opacity: `${num / 10}` })],
    // 左上角圆角
    [
      /^rounded-tl-(\d+)$/,
      ([_, num]) => ({ 'border-top-left-radius': `${num}px` }),
    ],
    // 右上角圆角
    [
      /^rounded-tr-(\d+)$/,
      ([_, num]) => ({ 'border-top-right-radius': `${num}px` }),
    ],
    // 左下角圆角
    [
      /^rounded-bl-(\d+)$/,
      ([_, num]) => ({ 'border-bottom-left-radius': `${num}px` }),
    ],
    // 右下角圆角
    [
      /^rounded-br-(\d+)$/,
      ([_, num]) => ({ 'border-bottom-right-radius': `${num}px` }),
    ],
    // 边框
    [/^b-width-(\d+)$/, ([_, num]) => ({ 'border-width': `${num}px` })],
    // 边框颜色
    [/^b-color-(\w+)$/, ([_, color]) => ({ 'border-color': `#${color}` })],
    // 边框样式
    [/^b-style-(\w+)$/, ([_, style]) => ({ 'border-style': style })],
    // border-top
    [
      /^b-top-(\d+)-(\w+)$/,
      ([_, w, color]) => ({
        'border-width': '0',
        'border-top-width': `${w}px`,
        'border-top-color': `#${color}`,
        'border-top-style': 'solid',
      }),
    ],
    // border-top
    [
      /^b-bottom-(\d+)-(\w+)$/,
      ([_, w, color]) => ({
        'border-width': '0',
        'border-bottom-width': `${w}px`,
        'border-bottom-color': `#${color}`,
        'border-bottom-style': 'solid',
      }),
    ],
    [
      /^border-(\d+)-(\w+)$/,
      ([_, w, color]) => ({
        'border-width': `${w}px`,
        'border-color': `#${color}`,
        'border-style': 'solid',
      }),
    ],
    [
      /^border-bottom-(\d+)-(\w+)$/,
      ([_, w, color]) => ({
        'border-bottom': `${w}px solid #${color}`,
      }),
    ],
    // border-top
    [
      /^border-top-(\d+)-(\w+)$/,
      ([_, size, color]) => ({
        'border-top': `${size}px solid #${color}`,
      }),
    ],
    // border-right
    [
      /^border-right-(\d+)-(\w+)$/,
      ([_, size, color]) => ({
        'border-right': `${size}px solid #${color}`,
      }),
    ],
    [
      // triangle-w-x-h-y // 画一个三角形 宽x 高度y 用正则匹配
      /^triangle-w-(\d+)-h-(\d+)-color-(\w+)$/,
      ([_, w, h, color]) => ({
        width: 0,
        height: 0,
        'border-left': `${+w / 2}px solid transparent`,
        'border-right': `${+w / 2}px  solid transparent`,
        'border-top': `${h}px solid #${color}`,
      }),
    ],

    // ** 居中相关 **
    // 绝对水平垂直居中
    [
      'absolute-center',
      {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
      },
    ],
    // 绝对水平居中
    [
      'absolute-center-x',
      { position: 'absolute', left: '50%', transform: 'translateX(-50%)' },
    ],
    [
      'm-x-auto',
      {
        'margin-left': 'auto',
        'margin-right': 'auto',
      },
    ],

    // ** 2D转换 **
    [
      /^rotate-(\d+)$/,
      ([_, deg]) => ({
        transform: `rotate(${deg}deg)`,
        'transform-origin': 'center center',
      }),
    ],
    // ! 设置边框 delete
    [
      'border-test',
      {
        border: '1px solid #000',
      },
    ],

    [
      'o-hidden',
      {
        overflow: 'hidden',
      },
    ],
    // overflow-y
    [
      'o-y',
      {
        overflow: 'auto',
      },
    ],
    // inline-block
    [
      'inline-block',
      {
        display: 'inline-block',
      },
    ],
    // ** 动画/过渡 **
    // 开启过渡 transition-0.3 正则匹配0.3
    [
      /^transition-(\d+)$/,
      ([, d]) => ({
        transition: `all ${d}s ease`,
      }),
    ],
  ],
})
