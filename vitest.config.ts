import { defineConfig } from 'vitest/config'
import vue from '@vitejs/plugin-vue'
import tsconfig from 'vite-tsconfig-paths'

export default defineConfig({
  plugins: [vue(), tsconfig()],
  test: {
    environment: 'happy-dom',
    setupFiles: ['./tests/setup.ts'],
    include: ['./src/**/*.test.ts', './src/**/*.spec.ts'],
    coverage: {
      reportsDirectory: 'coverage',
    },
  },
})
